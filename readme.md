K7 - Design a baby

Lähteülesanne

Kirjeldus: Lapse välimust võib osaliselt mõjutada see, milliseid geenivariante kannavad tema vanemad. Mõned päritavad tunnused domineerivad teiste üle (tume juuksevärv). Teised tunnused avalduvad vaid siis, kui domineerivat tunnust ei esine (sinised silmad). Vali mängus vanematele tunnused ja vaata, milline võib tulla nende laps. 

Mängus saad valida, kas oeld Mees või Naine, enda kohta käivad erinevad tunnused ning siis ka vastaspoole tunnused ning nende põhjal arvutab programm välja laste erinevad tunnused. 

Mängu mootor põhineb etteantud r-scripti koodile koos exelis tabelis oleva tabeliga.

Kujundusfailide hulgas on beebi vektoris. Juukse ja nahavärvid saab naise-mehe failist kätte. Lõualohke, kõrvu ja muud kribu-krabu beebi pildile ei tule — muutuvad vaid nahk (nahaga koos ka nina ja suu), juuksed, silmad ja vastavalt soole teki värv.

Läbi näituse on kindlad 5 värvi:
Hele taust: #f1f1ec
Tekst, nupud ja mustad pinnad: #1d252f
Roosa: #e00075
Lilla: #252060
Helesinine: #94d3de


Mängu tööfailid asuvad Rackstationis: \\rackstation._smb._tcp.local\TMD\PROJEKTID\TELETORN\TTT_PR04_GeenivaramuEkspo\03 TÖÖFAILID\K7 Design a baby