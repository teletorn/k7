var lang;

/* DOCUMENT READY */
jQuery(document).ready(function(){

	// -- Get EST translations
	getTranslations('et');

	// -- Languages click function
	jQuery('.languages div').click(function(){
		lang = jQuery(this).attr('data-language');
		if(lang){
			getTranslations(lang);
		}
	});

	// -- Choice click
	jQuery('.choice span').click(function(){
		jQuery('.choice span[data-questionchoice="' + jQuery(this).attr('data-questionchoice') + '"]').removeClass('selected');
		jQuery('button[data-questionbtn="' + jQuery(this).attr('data-questionchoice') + '"]').addClass('visible');
		jQuery(this).addClass('selected');
	});

	// -- Load "naine" svg
	jQuery.get(window.location.href + 'images/person/Beebidisain_naine_ja_mees_Artboard_naine.svg', function(svg){
		jQuery('#female, #final_female').html(svg);
	}, 'text');

	// -- Load "mees" svg
	jQuery.get(window.location.href + 'images/person/Beebidisain_naine_ja_mees_Artboard_mees.svg', function(svg){
		jQuery('#men, #final_men').html(svg);
	}, 'text');

	// -- Load "baby" svg
	jQuery.get(window.location.href + 'images/person/Beebidisain_beebi_Beebi.svg', function(svg){
		jQuery('.final_baby_image, .sibling').html(svg);
	}, 'text');

	// -- Partner image click
	jQuery('.partner').click(function(){
		jQuery('.partner').removeClass('selectedPartner');
		jQuery(this).addClass('selectedPartner');
		jQuery('button[data-questionbtn="11"]').addClass('visible');
		setPartnerValues(jQuery(this).attr('data-partnerId'));
		setSelectedList();
		getTranslations(jQuery('.languages div.active').attr('data-language'));
	});

});

/* FUNCTIONS */

	// -- Img set gender
	function selectGender(gender){
		jQuery('#gender_trigger_' + gender).trigger('click');
	}

	// -- TRANSLATIONS
	function getTranslations(lang) {
		jQuery('.languages div').removeClass('active');
		jQuery('.languages div[data-language="' + lang + '"]').addClass('active');
		var json = getLangJSON(lang);
		jQuery.each(json[0], function (key, data) {
			jQuery('[data-translation="' + key + '"]').html(data).val(data);
		});
	}

	function getLangJSON(lang){
		var json;
		if(lang == 'et'){
			json = translations.et;
		} else if(lang == 'en') {
			json = translations.en;
		} else if(lang == 'ru') {
			json = translations.ru;
		} else if(lang == 'fi') {
			json = translations.fi;
		}
		return json;
	}

	// -- ON MESSAGE
	function onMessage(evt){
		try{

		}catch(Ex){
			console.log(Ex);
		}
	}

	// -- GO TO VIEW
	function goToView(viewNumber){
		//clearTimer();
		if(viewNumber == 3){
			$('#person_image_container').removeClass('hidden');
		}
		if(viewNumber == 12){
			$('#person_image_container').addClass('hidden');
			(is_p1_male) ? $('.partner_container[data-gender="female"]').removeClass('hidden') : $('.partner_container[data-gender="male"]').removeClass('hidden');
		}
		if(viewNumber == 13){
			(is_p1_male) ? $('.selected_person_final_image #final_men').removeClass('hidden') : $('.selected_person_final_image #final_female').removeClass('hidden');
			setfinalPartner();
		}
		if(viewNumber == 14){
			setTimeout(function(){
				goToView(15);
			}, 5000);
		}
		if(viewNumber == 15){
			setBabyData();
			getTranslations(jQuery('.languages div.active').attr('data-language'));
		}
		if(viewNumber == 16){
			setSiblings();
		}
		$(".view").removeClass("show");
		$(".view:nth-of-type("+viewNumber+")").addClass("show");
	}

	// -- Set Final Partner
	function setfinalPartner(){
		var image = $('.partner.selectedPartner img').attr('src');
		$('#final_partner_image').html('<img src="' + image + '" />');
	}

	// -- Set Partner data
	function setPartnerValues(partnerID){
		switch(partnerID) {
			case "1":
				set_p2(false, false, false, false, false, false, 'Brown', 'Black', 'Dark');
				break;
			case "2":
				set_p2(false, false, false, false, false, false, 'Brown', 'Black', 'Brown');
				break;
			case "3":
				set_p2(false, false, false, false, false, false, 'Blue', 'Blonde', 'Light');
				break;
			case "4":
				set_p2(false, false, false, false, false, false, 'Green', 'Blonde', 'Light');
				break;
			case "5":
				set_p2(false, false, false, false, false, false, 'Green', 'Red', 'Light');
				break;
			case "6":
				set_p2(false, false, false, false, false, false, 'Blue', 'Brown', 'Light');
				break;
		}
	}

	// -- Set Selected List
	function setSelectedList(){
		$('.spf_view[data-no="1"] .selected_list').html('<span data-translation="final_skin_' + p1_skin  + '"></span><span data-translation="final_hair_' + p1_hair  + '"></span><span data-translation="final_curlyhair_' + has_p1_curlyHair  + '"></span><span data-translation="final_eyes_' + p1_eyes  + '"></span><span data-translation="final_earlobe_' + has_p1_earlobe  + '"></span><span data-translation="final_cleftchin_' + has_p1_cleftChin  + '"></span><span data-translation="final_freckles_' + has_p1_freckles  + '"></span><span data-translation="final_dimples_' + has_p1_dimples  + '"></span><span data-translation="final_rollable_tongue_' + has_p1_rollableTongue  + '"></span>');
		$('.spf_view[data-no="2"] .selected_list').html('<span data-translation="final_skin_' + p2_skin  + '"></span><span data-translation="final_hair_' + p2_hair  + '"></span><span data-translation="final_curlyhair_' + has_p2_curlyHair  + '"></span><span data-translation="final_eyes_' + p2_eyes  + '"></span><span data-translation="final_earlobe_' + has_p2_earlobe  + '"></span><span data-translation="final_cleftchin_' + has_p2_cleftChin  + '"></span><span data-translation="final_freckles_' + has_p2_freckles  + '"></span><span data-translation="final_dimples_' + has_p2_dimples  + '"></span><span data-translation="final_rollable_tongue_' + has_p2_rollableTongue  + '"></span>');
	}

	// -- Set Baby parameters
	function setBabyData(){
		var child = getChildren();
		//console.log(child);
		$('#final_baby .selected_list').html('<span data-translation="you_got_a_' + child[0].gender  + '"></span><span data-translation="final_skin_' + child[0].skinColour  + '"></span><span data-translation="final_hair_' + child[0].hairColour  + '"></span><span data-translation="final_curlyhair_' + child[0].curlyHair  + '"></span><span data-translation="final_eyes_' + child[0].eyeColour  + '"></span><span data-translation="final_earlobe_' + child[0].earlobe  + '"></span><span data-translation="final_cleftchin_' + child[0].cleftChin  + '"></span><span data-translation="final_freckles_' + child[0].freckles  + '"></span><span data-translation="final_dimples_' + child[0].dimples  + '"></span><span data-translation="final_rollable_tongue_' + child[0].rollableTonge  + '"></span>');
		$('.Beebi .Silmad').attr('selection', child[0].eyeColour);
		$('.Beebi .Nahavarv').attr('selection', child[0].skinColour);
		$('.Beebi .Juuksed').attr('selection', child[0].hairColour);
		$('.Beebi .Nina_ja_suu').attr('selection', child[0].skinColour);
		$('.Beebi .Sugu').attr('selection', child[0].gender);
	}

	// -- Set Siblings
	function setSiblings(){
		var child = getChildren();
		setSiblingValues(child, '1');
		setSiblingValues(child, '2');
		setSiblingValues(child, '3');
	}

	// -- Set Siblings function
	function setSiblingValues(child, no){
		$('.sibling[data-no="' + no + '"] .Beebi .Silmad').attr('selection', child[no].eyeColour);
		$('.sibling[data-no="' + no + '"] .Beebi .Nahavarv').attr('selection', child[no].skinColour);
		$('.sibling[data-no="' + no + '"] .Beebi .Juuksed').attr('selection', child[no].hairColour);
		$('.sibling[data-no="' + no + '"] .Beebi .Nina_ja_suu').attr('selection', child[no].skinColour);
		$('.sibling[data-no="' + no + '"] .Beebi .Sugu').attr('selection', child[no].gender);
	}