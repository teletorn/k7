// Program flow:
// 1) define variables that need input
// 2) define data frames of phenotype probabilities (same as in phenotypeProbabilites.xlsx)
// 3) define function that returns child phenotype and possible alternative phenotypes according to input
// 4) Use defined function to return child phenotype (single values, character)
// 5) Use defined function to return 5 possible alternative phenotypes (data.frame, all character)

let numOfObjectsToCreate = 4;

var is_p1_male = false;

var started = false;

// 1) Do parents (p1 and p2) have dimples - logical; TRUE if parent has dimples.
// Dominance order: parent doesn't have dimples < parent has dimples
var has_p1_dimples = false;
var has_p2_dimples = false;

// 2) Do parents (p1 and p2) have earlobes - logical; TRUE if parent has earlobes.
//Dominance order: parent doesn't have earlobes < parent has earlobes
var has_p1_earlobe = false;
var has_p2_earlobe = false;

// 3) Do parents (p1 and p2) have cleft chin - logical; TRUE if parent has cleft chin.
// Dominance order: parent doesn't have cleft chin < parent has cleft chin
var has_p1_cleftChin = false;
var has_p2_cleftChin = false;

// 4) Do parents (p1 and p2) have straight or curly hair - logical; TRUE if parent has curly hair.
// Dominance order: parent has straight hair < parent doesn't haves straight hair
var has_p1_curlyHair = false;
var has_p2_curlyHair = false;

// 5) Do parents (p1 and p2) have freckles - logical; TRUE if parent has freckles.
// Dominance order: parent doesn't have freckles < parent has freckles
var has_p1_freckles = false;
var has_p2_freckles = false;

// 6) Can parents (p1 and p2) roll their tongue - logical; TRUE if can roll tongue.
// Dominance order: parent can't roll tongue < parent can roll tongue
var has_p1_rollableTongue = false;
var has_p2_rollableTongue = false;

// 7) Eye colours for parents - character; Three options: "Brown", "Green", "Blue"
// Dominance order for eyes: Brown > Green > Blue #
var p1_eyes = "Blue";
var p2_eyes = "Brown";

// 8) Hair colours for parents - character; Three options: "Black", Brown", "Blonde", "Red"
// Dominance order for hair: Black > Brown > Blonde > Red #
var p1_hair = "Red";
var p2_hair = "Blonde";

// 9) Skin colours for parents - character; Three options: "Dark", Brown", "Light"
// Dominance order for skin colour: Dark > Brown > Light #
var p1_skin = "Dark";
var p2_skin = "Light";


function set_p1_gender(is_male){
    is_p1_male = is_male;
    $('#men, #female').addClass('hidden');
    (is_p1_male) ? $('#men').removeClass('hidden') : $('#female').removeClass('hidden');
}

function set_p1_dimples(dimples){
    has_p1_dimples = dimples;
    $('.Poselohud').attr("selection", dimples).attr('color', $('.Nahavarv').attr('selection'));
}

function set_p1_earlobe(earlobe){
    has_p1_earlobe = earlobe;
    $('.Korv_nibuga, .Korv_ilma_nibuta').removeAttr('selection').removeAttr('color');
    $('.Korv_nibuga, .Korv_ilma_nibuta').attr('color', $('.Nahavarv').attr('selection'));
    (earlobe) ? $('.Korv_nibuga').attr("selection", 'true') : $('.Korv_ilma_nibuta').attr("selection", 'true');
}

function set_p1_cleftChin(cleftChin){
    has_p1_cleftChin = cleftChin;
    $('.Loualohk').attr("selection", cleftChin).attr('color', $('.Nahavarv').attr('selection'));

}

function set_p1_curlyHair(curlyHair){
    has_p1_curlyHair = curlyHair;
    $('.Lokid').attr("selection", curlyHair);
}

function set_p1_freckles(freckles){
    has_p1_freckles = freckles;
    $('.Tedretapid').attr("selection", freckles).attr('color', $('.Nahavarv').attr('selection'));
}

function set_p1_rollableTongue(rollableTonge){
    has_p1_rollableTongue = rollableTonge;
}

function set_p1_eyes(eyes) {
    p1_eyes = eyes;
    $('.Silmad').attr("selection", eyes);
}

function set_p1_hair(hair){
    p1_hair = hair;
    $('.Juuksed').attr("selection", hair);
}

function set_p1_skin(skin){
    p1_skin = skin;
    $('.Nahavarv').attr("selection", skin);
}

function set_p2(dimples, earlobe, cleftChin, curlyHair, freckles, rollableTonge, eyes, hair, skin){
    has_p2_dimples = dimples;
    has_p2_earlobe = earlobe;
    has_p2_cleftChin = cleftChin;
    has_p2_curlyHair = curlyHair;
    has_p2_freckles = freckles;
    has_p2_rollableTongue = rollableTonge;
    p2_eyes = eyes;
    p2_hair = hair;
    p2_skin = skin;
}

function clearVariables(){
    started = false;
    p1_eyes = "";
    p1_hair = "";
    p1_skin = "";
    p2_eyes = "";
    p2_hair = "";
    p2_skin = "";
}

/* 2) SECTION: Define data frames for child phenotype probabilities
#--------------------------------------------------------#
#                                                        #
#          DEFINE PARENTAL PHENOTYPES FOR INPUT          #
#                                                        #
#--------------------------------------------------------# */

// Probabilities for binary traits (p1 - parent1, p2 - parent2, combinations both ways included) #
let binary = [
    {"p1":true,"p2":true,"p_TRUE":0.89,"p_FALSE":0.11},
    {"p1":true,"p2":false,"p_TRUE":0.67,"p_FALSE":0.33},
    {"p1":false,"p2":true,"p_TRUE":0.67,"p_FALSE":0.33},
    {"p1":false,"p2":false,"p_TRUE":0,"p_FALSE":1}
];

// Probabilities for eye colour (p1 - parent1, p2 - parent2, combinations both ways included) #
let eyes = [
    {"p1":"Brown","p2":"Brown","p_Brown":0.75,"p_Green":0.1875,"p_Blue":0.0625},
    {"p1":"Brown","p2":"Blue","p_Brown":0.5,"p_Green":0,"p_Blue":0.5},
    {"p1":"Brown","p2":"Green","p_Brown":0.5,"p_Green":0.375,"p_Blue":0.125},
    {"p1":"Blue","p2":"Brown","p_Brown":0.5,"p_Green":0,"p_Blue":0.5},
    {"p1":"Blue","p2":"Blue","p_Brown":0,"p_Green":0.01,"p_Blue":0.99},
    {"p1":"Blue","p2":"Green","p_Brown":0,"p_Green":0.5,"p_Blue":0.5},
    {"p1":"Green","p2":"Brown","p_Brown":0.5,"p_Green":0.375,"p_Blue":0.125},
    {"p1":"Green","p2":"Blue","p_Brown":0,"p_Green":0.5,"p_Blue":0.5},
    {"p1":"Green","p2":"Green","p_Brown":0.05,"p_Green":0.75,"p_Blue":0.2}
];

// Probabilities for hair colour (p1 - parent1, p2 - parent2, combinations both ways included) #
let hair = [
    {"p1":"Black","p2":"Black","p_Black":1,"p_Brown":0,"p_Blonde":0,"p_Red":0},
    {"p1":"Black","p2":"Brown","p_Black":0.5,"p_Brown":0.5,"p_Blonde":0,"p_Red":0},
    {"p1":"Black","p2":"Blonde","p_Black":0,"p_Brown":0.5,"p_Blonde":0,"p_Red":0.5},
    {"p1":"Black","p2":"Red","p_Black":0,"p_Brown":1,"p_Blonde":0,"p_Red":0},
    {"p1":"Brown","p2":"Black","p_Black":0.5,"p_Brown":0.5,"p_Blonde":0,"p_Red":0},
    {"p1":"Brown","p2":"Brown","p_Black":0.25,"p_Brown":0.5,"p_Blonde":0.12,"p_Red":0.13},
    {"p1":"Brown","p2":"Blonde","p_Black":0,"p_Brown":0.5,"p_Blonde":0.34,"p_Red":0.16},
    {"p1":"Brown","p2":"Red","p_Black":0,"p_Brown":0.5,"p_Blonde":0,"p_Red":0.5},
    {"p1":"Blonde","p2":"Black","p_Black":0,"p_Brown":1,"p_Blonde":0,"p_Red":0},
    {"p1":"Blonde","p2":"Brown","p_Black":0,"p_Brown":0.5,"p_Blonde":0.34,"p_Red":0.16},
    {"p1":"Blonde","p2":"Blonde","p_Black":0,"p_Brown":0,"p_Blonde":1,"p_Red":0},
    {"p1":"Blonde","p2":"Red","p_Black":0,"p_Brown":0,"p_Blonde":0,"p_Red":1},
    {"p1":"Red","p2":"Black","p_Black":0,"p_Brown":1,"p_Blonde":0,"p_Red":0},
    {"p1":"Red","p2":"Brown","p_Black":0,"p_Brown":0.5,"p_Blonde":0,"p_Red":0.5},
    {"p1":"Red","p2":"Blonde","p_Black":0,"p_Brown":0,"p_Blonde":0,"p_Red":1},
    {"p1":"Red","p2":"Red","p_Black":0,"p_Brown":0,"p_Blonde":0,"p_Red":1}
];

// Probabilities for skin colour (p1 - parent1, p2 - parent2, combinations both ways included) #
let skin = [
    {"p1":"Dark","p2":"Dark","p_Dark":1,"p_Brown":0,"p_Light":0},
    {"p1":"Dark","p2":"Brown","p_Dark":0.5,"p_Brown":0.5,"p_Light":0},
    {"p1":"Dark","p2":"Light","p_Dark":0,"p_Brown":1,"p_Light":0},
    {"p1":"Brown","p2":"Dark","p_Dark":0.5,"p_Brown":0.5,"p_Light":0},
    {"p1":"Brown","p2":"Brown","p_Dark":0,"p_Brown":1,"p_Light":0},
    {"p1":"Brown","p2":"Light","p_Dark":0,"p_Brown":0.5,"p_Light":0.5},
    {"p1":"Light","p2":"Dark","p_Dark":0,"p_Brown":1,"p_Light":0},
    {"p1":"Light","p2":"Brown","p_Dark":0,"p_Brown":0.5,"p_Light":0.5},
    {"p1":"Light","p2":"Light","p_Dark":0,"p_Brown":0,"p_Light":1}
];

let genders = ["male", "female"];




/* 3) SECTION: Define function to select child phenotype according to parents phenotype
#-----------------------------------------------------------------#
#                                                                 #
#          DEFINE FUNCTION FOR CHILD PHENOTYPE SELECTION          #
#                                                                 #
#-----------------------------------------------------------------# */

//-----------------
// p1_pheno - phenotype for parent 1, as defined above, migh be logical or character
// p2_pheno - phenotype for parent 2, as defined above, migh be logical or character
// dataset - one of four (binary, eyes, hair, skin) as defined above
//------------------

function findChildPhenotype(p1_pheno, p2_pheno, dataset){
    var phenotypes = [];
    var maxPhenotypes = [];
    switch (dataset) {
        case "binary":
            for (var i = 0; i < binary.length; i++){
                let row = binary[i];
                if (row.p1 == p1_pheno && row.p2 == p2_pheno){
                    if (row.p_TRUE > 0){
                        maxPhenotypes.push(true);
                        phenotypes.push(false);
                        phenotypes.push(true);
                    } else {
                        maxPhenotypes.push(false);
                        phenotypes.push(false);
                    }
                }
            }
            break;
        case "eyes":
            for (var i = 0; i < eyes.length; i++){
                let row = eyes[i];
                if (row.p1 == p1_pheno && row.p2 == p2_pheno){
                    let max = Math.max(row.p_Brown, row.p_Green, row.p_Blue);
                    if (row.p_Brown > 0){
                        phenotypes.push("Brown");
                        if (row.p_Brown == max) maxPhenotypes.push("Brown");
                    }
                    if (row.p_Green > 0){
                        phenotypes.push("Green");
                        if (row.p_Green == max) maxPhenotypes.push("Green");
                    }
                    if (row.p_Blue > 0){
                        phenotypes.push("Blue");
                        if (row.p_Blue == max) maxPhenotypes.push("Blue");
                    }
                }
            }
            break;
        case "hair":
            for (var i = 0; i < hair.length; i++){
                let row = hair[i];
                if (row.p1 == p1_pheno && row.p2 == p2_pheno){
                    let max = Math.max(row.p_Black, row.p_Brown, row.p_Blonde, row.p_Red);
                    if (row.p_Black > 0){
                        phenotypes.push("Black");
                        if (row.p_Black == max) maxPhenotypes.push("Black");
                    }
                    if (row.p_Brown > 0){
                        phenotypes.push("Brown");
                        if (row.p_Brown == max) maxPhenotypes.push("Brown");
                    }
                    if (row.p_Blonde > 0){
                        phenotypes.push("Blonde");
                        if (row.p_Blonde == max) maxPhenotypes.push("Blonde");
                    }
                    if (row.p_Red > 0){
                        phenotypes.push("Red");
                        if (row.p_Red == max) maxPhenotypes.push("Red");
                    }
                }
            }
            break;
        case "skin":
            for (var i = 0; i < skin.length; i++){
                let row = skin[i];
                if (row.p1 == p1_pheno && row.p2 == p2_pheno){
                    let max = Math.max(row.p_Dark, row.p_Brown, row.p_Light);
                    if (row.p_Dark > 0){
                        phenotypes.push("Dark");
                        if (row.p_Dark == max) maxPhenotypes.push("Dark");
                    }
                    if (row.p_Brown > 0){
                        phenotypes.push("Brown");
                        if (row.p_Brown == max) maxPhenotypes.push("Brown");
                    }
                    if (row.p_Light > 0){
                        phenotypes.push("Light");
                        if (row.p_Light == max) maxPhenotypes.push("Light");
                    }
                }
            }
            break;
        default:
            break;
    }
    return [maxPhenotypes, phenotypes];
}

function getRandomElement(list){
    return list[Math.floor(Math.random()*list.length)];
}

function CombinationExists(list, newObj){
    for (var i = 0; i < list.length; i++){
        let obj = list[i];
        if (JSON.stringify(obj) == JSON.stringify(newObj)) return true;
    }
    return false;
}

function getChildren(){
        /* 4) SECTION: Define data frames for child phenotype probabilities
        #--------------------------------------------------------#
        #                                                        #
        #          DEFINE PARENTAL PHENOTYPES FOR INPUT          #
        #                                                        #
        #--------------------------------------------------------# */

        // Call the function that returns 1 list with the most likely outcome and the another list with all the possible outcomes.
        var child_dimples = findChildPhenotype(p1_pheno = has_p1_dimples, p2_pheno = has_p2_dimples, dataset = "binary");
        var child_earlobe = findChildPhenotype(p1_pheno = has_p1_earlobe, p2_pheno = has_p2_earlobe, dataset = "binary");
        var child_cleftChin = findChildPhenotype(p1_pheno = has_p1_cleftChin, p2_pheno = has_p2_cleftChin, dataset = "binary");
        var child_curlyHair = findChildPhenotype(p1_pheno = has_p1_curlyHair, p2_pheno = has_p2_curlyHair, dataset = "binary");
        var child_freckles = findChildPhenotype(p1_pheno = has_p1_freckles, p2_pheno = has_p2_freckles, dataset = "binary");
        var child_rollableTongue = findChildPhenotype(p1_pheno = has_p1_rollableTongue, p2_pheno = has_p2_rollableTongue, dataset = "binary");

        var child_eyeColour = findChildPhenotype(p1_pheno = p1_eyes, p2_pheno = p2_eyes, dataset = "eyes");
        var child_hairColour = findChildPhenotype(p1_pheno = p1_hair, p2_pheno = p2_hair, dataset = "hair");
        var child_skinColour = findChildPhenotype(p1_pheno = p1_skin, p2_pheno = p2_skin, dataset = "skin");


        //Get the most likely outcome
        var output_child_dimples = getRandomElement(child_dimples[0]);
        var output_child_earlobe = getRandomElement(child_earlobe[0]);
        var output_child_cleftChin = getRandomElement(child_cleftChin[0]);
        var output_child_curlyHair = getRandomElement(child_curlyHair[0]);
        var output_child_freckles = getRandomElement(child_freckles[0]);
        var output_child_rollableTongue = getRandomElement(child_rollableTongue[0]);

        var output_child_eyeColour = getRandomElement(child_eyeColour[0]);
        var output_child_hairColour = getRandomElement(child_hairColour[0]);
        var output_child_skinColour = getRandomElement(child_skinColour[0]);


        /* 5) SECTION: Select randomly 5 other possible child phenotypes
        #---------------------------------------------------------------------#
        #                                                                     #
        #          SELECT RANDOMLY 5 POSSIBLE PHENOTYPE COMBINATIONS          #
        #                                                                     #
        #---------------------------------------------------------------------# */


        var mostLikely = {
            "gender": getRandomElement(genders),
            "dimples": output_child_dimples,
            "earlobe": output_child_earlobe,
            "cleftChin": output_child_cleftChin,
            "curlyHair": output_child_curlyHair,
            "freckles": output_child_freckles,
            "rollableTonge": output_child_rollableTongue,
            "eyeColour": output_child_eyeColour,
            "hairColour": output_child_hairColour,
            "skinColour": output_child_skinColour
        };
        var possiblePhenotypeCombinations = [mostLikely];

        //console.log(mostLikely);

        //create 4 more random combinations
        while(possiblePhenotypeCombinations.length < numOfObjectsToCreate){
            var newOption = {
                "gender": getRandomElement(genders),
                "dimples": getRandomElement(child_dimples[1]),
                "earlobe": getRandomElement(child_earlobe[1]),
                "cleftChin": getRandomElement(child_cleftChin[1]),
                "curlyHair": getRandomElement(child_curlyHair[1]),
                "freckles": getRandomElement(child_freckles[1]),
                "rollableTonge": getRandomElement(child_rollableTongue[1]),
                "eyeColour": getRandomElement(child_eyeColour[1]),
                "hairColour": getRandomElement(child_hairColour[1]),
                "skinColour": getRandomElement(child_skinColour[1])
            }
            if (!CombinationExists(possiblePhenotypeCombinations, newOption)){
                possiblePhenotypeCombinations.push(newOption);
            }
        }

        //console.log(possiblePhenotypeCombinations);

        return possiblePhenotypeCombinations;

}

// getChildren();